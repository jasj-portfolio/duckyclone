#include <ezButton.h>
#include <Digitalkeyboard.h>

ezButton toggleSwitch();  // create ezButton object pass pin number as argument;

void setup() {
    Serial.begin(9600);
    toggleSwitch.setDebounceTime(50); // set debounce time to 50 milliseconds
}

void ducky(){
    // payload goes here
}

void loop() {
    toggleSwitch.loop();

    int state = toggleSwitch.getState();
    if (state == HIGH){
        return 0;
    }
    else{
        ducky();
    }
}
